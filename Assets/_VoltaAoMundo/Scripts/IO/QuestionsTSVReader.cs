﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestionType
{
    None,
    TXT,
    IMG,
    INT
}

public enum SpaceTextType
{
    None,
    Positive,
    Negative,
    Introduction
}

[System.Serializable]
public struct Question
{
    public List<int> Spaces;
    public QuestionType Type;
    public string Text;
    public List<string> WrongAnswers;
    public string RightAnswer;
    public int NPCIndex;
    public string AudioTag;
    public AudioClip Audio;

    public Question(List<int> spaces, QuestionType type, string text, List<string> wrongAnswers, string rightAnswer, int npcIndex, string audioTag)
    {
        Spaces = spaces;
        Type = type;
        Text = text;
        WrongAnswers = wrongAnswers;
        RightAnswer = rightAnswer;
        NPCIndex = npcIndex;
        AudioTag = audioTag;
        Audio = new AudioClip();
    }
}

[System.Serializable]
public struct SapceText
{
    public int Space;
    public SpaceTextType Type;
    public string Text;
    public string AudioTag;

    public SapceText(int space, SpaceTextType type, string text, string audioTag)
    {
        Space = space;
        Type = type;
        Text = text;
        AudioTag = audioTag;
    }
}

public class QuestionsTSVReader : MonoBehaviour {

    public TextAsset TSV;
    public TextAsset TSV2;
    public TextAsset TSV3;
    public TextAsset SpaceTextTSV;
    //public List<Question> Questions = new List<Question>();
    public List<List<Question>> LevelQuestions = new List<List<Question>>();
    public List<SapceText> SpaceTexts = new List<SapceText>();

    private static QuestionsTSVReader _instance;
    public static QuestionsTSVReader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<QuestionsTSVReader>();

            return _instance;
        }
    }

    private void Awake()
    {
        LevelQuestions.Add(ReadTSV(TSV));
        LevelQuestions.Add(ReadTSV(TSV2));
        LevelQuestions.Add(ReadTSV(TSV3));
        ReadSpaceTSV();
    }

    public List<Question> ReadTSV(TextAsset tsv)
    {
        List<Question> questions = new List<Question>();

        string[] lines = tsv.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);

            List<int> spaces = null;
            if (!string.IsNullOrEmpty(contents[0].Trim()))
            {
                string[] s = contents[0].Split(',');
                for (int j = 0; j < s.Length; j++)
                {
                    if (spaces == null)
                        spaces = new List<int>();

                    spaces.Add(int.Parse(s[j].Trim()));
                }
            }

            if (string.IsNullOrEmpty(contents[1].Trim()))
                continue;

            QuestionType type = (QuestionType)System.Enum.Parse(typeof(QuestionType), contents[1].Trim().ToUpper());

            string text = contents[2].Trim();

            List<string> wrongAnswers = new List<string>();
            for (int j = 0; j < 3; j++)
            {
                int index = 3 + j;
                if (!string.IsNullOrEmpty(contents[index].Trim()))
                    wrongAnswers.Add(contents[index].Trim());
            }

            string rightAnswer = contents[6].Trim();

            //int npcIndex = int.Parse(contents[8].Trim());
            int npcIndex = Random.Range(0, 6);

            string audioTag = "";

            questions.Add(new Question(spaces, type, text, wrongAnswers, rightAnswer, npcIndex, audioTag));
        }

        return questions;
    }

    private void ReadSpaceTSV()
    {
        string[] spaceLines = SpaceTextTSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < spaceLines.Length; i++)
        {
            string[] contents = spaceLines[i].Split("\t"[0]);

            int space = int.Parse(contents[0].Trim());

            SpaceTextType type = SpaceTextType.None;
            switch (contents[1].Trim())
            {
                case "A":
                    type = SpaceTextType.Positive;
                    break;
                case "E":
                    type = SpaceTextType.Negative;
                    break;
                case "I":
                    type = SpaceTextType.Introduction;
                    break;
                default:
                    break;
            }

            string text = contents[2].Trim();
            string audioTag = contents[3].Trim();

            SpaceTexts.Add(new SapceText(space, type, text, audioTag));
        }
    }

    public List<Question> GetQuestionsFromSpace(int space, int difficulty = 1)
    {
        List<Question> Questions = LevelQuestions[difficulty - 1];
        List<Question> questions = new List<Question>();

        for (int i = 0; i < Questions.Count; i++)
        {
            if ((Questions[i].Spaces == null || Questions[i].Spaces.Count == 0 || Questions[i].Spaces.Contains(space)) && !string.IsNullOrEmpty(Questions[i].Text) && !string.IsNullOrEmpty(Questions[i].RightAnswer))
            //if (Questions[i].Type == QuestionType.INT && !string.IsNullOrEmpty(Questions[i].RightAnswer))
            //if (Questions[i].Type == QuestionType.IMG && !string.IsNullOrEmpty(Questions[i].RightAnswer))
                questions.Add(Questions[i]);
        }

        return questions;
    }

    public SapceText GetSpaceText(int space, SpaceTextType type)
    {
        print(space + " " + type);

        for (int i = 0; i < SpaceTexts.Count; i++)
        {
            if (SpaceTexts[i].Space == space && SpaceTexts[i].Type == type)
                return SpaceTexts[i];
        }

        return new SapceText();
    }
}
