﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public static class GameConfigs
{
    public static int Level;
    public static bool Time;
}

public class SetupSceneManager : MonoBehaviour {
    
    public Toggle Level1Toggle;
    public Toggle Level2Toggle;
    public Toggle Level3Toggle;
    public Toggle TimeToggle;

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;

            return _audioSource;
        }
    }

    private void Start()
    {
        Input.backButtonLeavesApp = true;

        SetLevel1(Level1Toggle.isOn);
        SetLevel2(Level2Toggle.isOn);
        SetLevel3(Level3Toggle.isOn);
        SetTime(TimeToggle.isOn);

        //StartCoroutine(PlayAudio());
    }

    public void SetLevel1(bool enable)
    {
        if (enable)
            GameConfigs.Level = 1;
    }

    public void SetLevel2(bool enable)
    {
        if (enable)
            GameConfigs.Level = 2;
    }

    public void SetLevel3(bool enable)
    {
        if (enable)
            GameConfigs.Level = 3;
    }

    public void SetLevel4(bool enable)
    {
        if (enable)
            GameConfigs.Level = 4;
    }

    public void SetTime(bool enable)
    {
        GameConfigs.Time = TimeToggle.isOn;
    }

    public void Ready()
    {
        SceneManager.LoadScene(1);
    }

    IEnumerator PlayAudio()
    {
        yield return new WaitForSeconds(0.5f);

        AudioClip clip = Resources.Load<AudioClip>("Voice/NPC" + Random.Range(1, 7) + "/Config");
        AudioSource.PlayOneShot(clip);
    }
}
